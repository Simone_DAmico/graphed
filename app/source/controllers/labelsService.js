// language=AngularJS
app

// service per l'accesso alle label presenti nel grafo, ai nodi e ai links (numero ...)
	.service('Labels', function(){

	    this.nomeProgetto = "progetto senza nome";
	    this.nomeDB = null;
	    this.descrizioneProgetto = null;

	    this.pp={link_1: ["ci","c2","c3"]};

		this.nodes = [
		    {id: 0, reflexive: false, nome: "nodo1", proprieta: [{nomeProp: "nomeProp1-1", dominio: "int", valori: {default: "1", intervalli:[]}},
															{nomeProp: "nomeProp1-2", dominio: "bool", valori: []},
                                                            {nomeProp: "nomeProp1-3", dominio: "int", valori: []},
                                                            {nomeProp: "nomeProp1-4", dominio: "int", valori: []},
                                                            {nomeProp: "nomeProp1-5", dominio: "int", valori: []}],
                                                    primaryKey:[], secondarykey:[]},

			{id: 1, reflexive: false, nome: "nodo2", proprieta: [{nomeProp: "nomeProp2-1", dominio: "int", valori: []},
                											{nomeProp: "nomeProp2-2", dominio: "date", valori: []}],
                                                    primaryKey:[], secondarykey:[]},

			{id: 2, reflexive: false, nome: "nodo3", proprieta: [], primaryKey:[], secondarykey:[]}
			];


	    this.links = [
	        {id: 0, source: this.nodes[0], target: this.nodes[1], left: false, right: true, nome:"link_1",
                molteplicita: {source: "0 - N", target: "0 - N"}, foreing: {source: null, target: null},
                proprieta: [{nomeProp: "nomeProplink1-1", dominio: "int", valori: {default: "12", intervalli:[]}},
                            {nomeProp: "nomeProplink1-2", dominio: "bool", valori: []}] },

            {id: 1, source: this.nodes[0], target: this.nodes[2], left: false, right: true, nome:"link_2",
                molteplicita: {source: [], target: []}, foreing: {source: [], target: []},
                proprieta: [{nomeProp: "nomeProplink2-1", dominio: "float", valori: {default: "5.64", intervalli:[]}}] },

        	{id: 2, source: this.nodes[1], target: this.nodes[2], left: false, right: true, nome:"link_3",
                molteplicita: {source: [], target: []}, foreing: {source: [], target: []},
                proprieta: [] }
        	];

        this.lastNodeId = this.nodes.length - 1;
        this.lastLinkId = this.links.length - 1;

	    this.tipiBase=["int", "float", "string", "bool", "date", "enum"];

	    this.domini=[{nome: "int", generico: "int", default: null, valori: []},
                    {nome: "float", generico: "float", default: null, valori: []},
                    {nome: "string", generico: "string", default: null, valori: []},
                    {nome: "bool", generico: "bool", default: null, valori: []},
                    {nome: "date", generico: "date", default: null, valori: []},
                    {nome: "dom1", generico: "int", default: 5, valori: ["value > 2"]},
                    {nome: "sesso", generico: "enum", default: "Maschio", valori: ["Maschio", "Femmina"]}];

        this.operatoriConfronto = ["uguale","diverso","minore","maggiore","minore-uguale","maggiore-uguale"];

        this.molteplicita=["0 - 1", "0 - N", "1 - 1", "1 - N", "N - N"];

	    this.nSel=this.nodes[0];
        this.lSel=this.links[0];


        this.aggiungiLabel = function (label) {
            this.nodes.push(label);
        };

        this.aggiungiLink = function (link) {
            this.nodes.links(link);
        };

        this.aggiungitipiBase = function (dom) {
        	this.tipiBase.push(dom);
		};
	});