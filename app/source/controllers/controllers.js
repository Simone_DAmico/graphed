//language=Apple JS
app

// Controller del menu
.controller("BottoniMenu", function ($scope, $http, $mdDialog, Labels) {

    $scope.nomeProgetto = Labels.nomeProgetto;

	$scope.nuovoProgetto = function() {
		//window.location='graphEd.html';
		$scope.nomeProgetto = prompt("inserisci nome progetto");
        console.log("sss",$scope.nomeProgetto);
	};
	$scope.eliminazione = function() {
		alert("Progetto eliminato");
	};
	$scope.salvataggio = function() {
            $http.post("/post-singolo", Labels.nodes)
                .success(function() {
                    alert("inviato correttamente!");
                })
                .error(function() {
                    alert("Si è verificato un errore!");
                });

        var json = JSON.stringify(Labels.nodes);
        console.log(json);
		//alert("Progetto salvato");
	};
	$scope.rinominaProgetto = function() {
		$scope.nomeProgetto = prompt("inserisci nome progetto");
	}
});
/**
 * Controller per l'aggiunta delle label dalla pagina html.
 *
 * add invocata al click del bottone "aggiungi label", verifica se la lavel esiste già, in tal caso aumenta il numero di
 * nodi con quell'etichetta; altrimenti aggiunge la nuova label alla lista.
 */
app.controller("tabLabels", function($scope, $http, Labels){
    $scope.add = function(){
        var keepGoing = false;
        var i;
        var newLabel = prompt("inserisci label");
        if (newLabel !== null) {
            Labels.labels.forEach(function(element){
                if (element.nome === newLabel) {
                    keepGoing = true;
                    i = Labels.labels.indexOf(element)
                }
            });

            if (newLabel === "" || newLabel === undefined) {
                alert("inserisci la label");
            } else if (keepGoing) {
                Labels.labels[i].numero++;
            } else {
                Labels.labels.push({nome: newLabel, numero: 1});
                Labels.lastNodeId = Labels.lastNodeId+1;
                Labels.nodes.push({nome:"nodo"+Labels.lastNodeId, label:newLabel, id: Labels.lastNodeId, reflexive: false});
                console.log(Labels.nodes);
            }
        }
    }
});

// controller della lista delle label del grafo presa dalla factory Labels
app.controller("labelList", function($scope, Labels){
    $scope.ll = Labels.labels;
});

app.controller("addDom", function ($scope, Labels) {
    //funzione per l'aggiunta di un nuovo dominio
    $scope.addDom = function() {
        var newDom = prompt("inserisci un nuovo dominio");
        Labels.aggiungiDomains(newDom);
    }
});
