app

// controller dei nodi del grafo; gestisce il model di ogni nodo
.controller("nodeController", function($scope, $mdDialog, Labels){

	// mostra il modal con le info del nodo selezionato
    $scope.showNodeInfo = function($event) {

        $mdDialog.show({
            targetEvent: $event,
            templateUrl: 'source/templates/nodeModal.html',
            windowClass: 'modal-content',
            controller: nodeCtrl,
            clickOutsideToClose: true
        });
    };

  // funzione che gastisce il modal
	function nodeCtrl($scope, $mdDialog, Labels) {

	    $scope.nodi = Labels.nodes;
        $scope.domini = Labels.domini;
        $scope.operatoriConfronto=Labels.operatoriConfronto;
        $scope.nodoSelezionato = $scope.nodi[0].nome;
        $scope.opConSel = Labels.operatoriConfronto[0];
        $scope.indiceSelezionato = 0;
        $scope.pk = null;
        $scope.vincDom= $scope.vincProp;
        $scope.vincTupla = [];

        //console.log($scope.newNodeProp.dominio);
        //console.log($scope.proprietaNodo.n.$invalid, $scope.proprietaNodo.d.$pristine);
        
        function indice(n) {
            return n.nome === $scope.nodoSelezionato
        }

        $scope.cambiaNodo = function () {
            $scope.indiceSelezionato = $scope.nodi.findIndex(indice);
        };

        function indiceProp(n) {
            return n.nome === $scope.nodoSelezionato
        }

        $scope.cambiaProp = function (ind) {
            $scope.propSelezionata = $scope.nodi[$scope.indiceSelezionato].proprieta[ind];
        };
        // Aggiunge la chiave primaria
        $scope.addPK = function () {
            if ($scope.nodi[$scope.indiceSelezionato].secondarykey.length > 0){

                $scope.nodi[$scope.indiceSelezionato].secondarykey.forEach(function (chiave) {
                    chiave.forEach(function (value) {
                        if (value === $scope.pk[0]) {
                            alert("non puoi scegliere come chiave primaria una proprietà che è gia chaive secondaria");
                            console.log($scope.nodi[$scope.indiceSelezionato].proprieta.nome);
                            return $scope.nodi[$scope.indiceSelezionato].primaryKey.indexOf($scope.pk[0]);
                        }
                    })
                })
            }
            else $scope.nodi[$scope.indiceSelezionato].primaryKey = $scope.pk;
        };

        // Verifica che la chiave primaria non sia già una chiave secondaria
        // Aggiunge la chiave secondaria
        $scope.addSK = function () {
            $scope.nodi[$scope.indiceSelezionato].secondarykey.push($scope.altraChiave);
            console.log("seco", $scope.nodi[$scope.indiceSelezionato].secondarykey);
            //console.log($scope.nodi[$scope.indiceSelezionato].primaryKey.indexOf($scope.prop.nomeProp)>-1);
            $scope.altraChiave = undefined;
        };

        // Rimuove la chiave secondaria tramite l'indice
        $scope.removeSK = function (ind) {
            $scope.nodi[$scope.indiceSelezionato].secondarykey.splice(ind, 1);
        };
        
        // funzione che permette l'aggiunta di una proprietà al nodo
        $scope.addNodeProp = function () {

            var keepGoing = true;

            // Se il nome della propietà non è stato inserito --> allert
            if ($scope.newNodeProp.nome === undefined || $scope.newNodeProp.nome === '' || $scope.newNodeProp.nome === null) {
                alert("Inserire il nome della proprietà");
                return;
            }

            // Controllo se il nome della proprietà esisste gia --> allert
            $scope.nodi[$scope.indiceSelezionato].proprieta.forEach(function (element) {
                if (element.nomeProp === $scope.newNodeProp.nome) {
                    alert("La proprietà esiste già");
                    keepGoing = false;
                }
            });
            // Se la proprietà non esiste già (unicità del nome) --> la inserisco
            if (keepGoing)
                $scope.nodi[$scope.indiceSelezionato].proprieta.push({nomeProp: $scope.newNodeProp.nome, dominio: $scope.newNodeProp.dominio});
            $scope.newNodeProp.nome = undefined;
            $scope.newNodeProp.dominio = undefined;
            console.log("dopo  ",$scope.proprietaNodo.n.$invalid, $scope.proprietaNodo.d.$pristine);
            console.log("sssss  ",Labels.nodes);
        };

        // Rimuove la proprieta all'indice ind
        $scope.removeNodeProp = function (ind) {
            $scope.nodi[$scope.indiceSelezionato].proprieta.splice(ind, 1);
        };


        // Nasconde il modal
        $scope.closeDialog = function () {
            $mdDialog.hide();
        };

        //Inserimento degli intervalli per le propriietà
        $scope.insIntervallo = function () {
            console.log($scope.nodi[$scope.indiceSelezionato].proprieta[$scope.vincProp].valori.default);
            //controllo estremi e definizione dell'intervallo
            if ($scope.sceltaVinc === "intervallo"){
                if ($scope.interDa == null || $scope.interA == null|| $scope.interDa === "" || $scope.interA === "") return;
                if ($scope.interDa <= $scope.interA) {
                    if ($scope.estremoDaIncl && $scope.estremoAIncl) var intervallo = "value >= " + $scope.interDa + " and value <= " + $scope.interA;

                    else if ($scope.estremoDaIncl && !$scope.estremoAIncl) var intervallo = "value >= " + $scope.interDa + " and value < " + $scope.interA;

                    else if (!$scope.estremoDaIncl && $scope.estremoAIncl) var intervallo = "value > " + $scope.interDa + " and value <= " + $scope.interA;

                    else var intervallo = "value > " + $scope.interDa + " and value < " + $scope.interA;
                }else{
                    alert("L'estremo inferiore deve essere minore o uguale di quello superiore");
                    return
                }
            }
            else if ($scope.sceltaVinc === "maggiore"){
                if ($scope.magDi == null|| $scope.magDi === "") return;
                if ($scope.estremoMag) var intervallo = "value >= " + $scope.magDi;
                else var intervallo = "value > " + $scope.magDi;
            }
            else if ($scope.sceltaVinc === "minore"){
                if ($scope.minDi == null || $scope.minDi === "") return;
                if ($scope.estremoMin) var intervallo = "value <= " + $scope.minDi;
                else var intervallo = "value < " + $scope.minDi;
            }

            // se il nuovo intervallo è gia presente non si inserisce
            console.log("dd", $scope.interA, $scope.interDa);
            if (($scope.nodi[$scope.indiceSelezionato].proprieta[$scope.vincProp].valori.intervalli.length === 0 ||
                !$scope.nodi[$scope.indiceSelezionato].proprieta[$scope.vincProp].valori.intervalli.includes(intervallo)) &&
                ($scope.interDa !== undefined || $scope.interA !== undefined || $scope.magDi !== undefined || $scope.minDi !== undefined))
                $scope.nodi[$scope.indiceSelezionato].proprieta[$scope.vincProp].valori.intervalli.push(intervallo);

            //reset campi degli intervalli
            $scope.interDa = null;
            $scope.estremoDaIncl = false;
            $scope.interA = null;
            $scope.estremoAIncl = false;
            $scope.magDi = null;
            $scope.estremoMag = false;
            $scope.minDi = null;
            $scope.estremoMin = false;
            console.log($scope.nodi[$scope.indiceSelezionato].proprieta[$scope.vincProp].valori);
        };

        // rimuove intervallo tramite indice
        $scope.removeIntervallo = function (ind) {
            $scope.nodi[$scope.indiceSelezionato].proprieta[$scope.vincProp].valori.intervalli.splice(ind, 1);
        };

        $scope.addVincoloTupla = function () {

            $scope.cla = $scope.propSelezionata.nomeProp + " " +$scope.operatore + " " + $scope.val;
            $scope.vincTupla.push($scope.cla);

            console.log("cla", $scope.cla);
            console.log("vincolo",$scope.vincTupla);
            console.log("not",$scope.not);
        };
        $scope.dd = function () {
            console.log("not",$scope.not);
            var auto = {"marca": "bmw", "modello": "X3", "colore": "blu", "posti": "5"};
            console.log("ss", auto["marca"]);
        }
	}
});