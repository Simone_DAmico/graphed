app

// controller dei nodi del grafo; gestisce il model di ogni nodo
    .controller("linkController", function($scope, $mdDialog, Labels){

        // mostra il modal con le info del link selezionato
        $scope.showLinkInfo = function($event) {
            $mdDialog.show({
                targetEvent: $event,
                templateUrl: 'source/templates/linkModal.html',
                windowClass: 'modal-content',
                controller: LinkCtrl,
                clickOutsideToClose: true
            });
        };

        function LinkCtrl($scope, $mdDialog, Labels) {

            $scope.links = Labels.links;
            $scope.domini = Labels.domini;
            $scope.molteplicita = Labels.molteplicita;
            $scope.linkSelezionato = $scope.links[0];

            $scope.operatoriConfronto=Labels.operatoriConfronto;
            $scope.opConSel = Labels.operatoriConfronto[0];
            $scope.pk = null;
            $scope.vincDom= $scope.vincProp;
            $scope.vincTupla = [];
            $scope.pp = Labels.pp;
            console.log($scope.pp);
            $scope.pp.link_2 = ["a1", "a2"];
            console.log("cc",$scope.molteplicita.indexOf($scope.linkSelezionato.molteplicita.source.min));
            console.log("ss",$scope.linkSelezionato.molteplicita.source.min);

            $scope.cambiaLink = function(){
                $scope.linkSelezionato = $scope.links[$scope.indexSelezionato];
            };

            $scope.addLinkProp = function () {

                var keepGoing = true;
                // Controllo se il nome della proprietà esisste gia --> allert
                $scope.linkSelezionato.proprieta.forEach(function (element) {
                    if (element.nomeProp === $scope.newLinkProp.nome) {
                        alert("La proprietà esiste già");
                        keepGoing = false;
                    }
                });
                // Se la proprietà non esiste --> la inserisco
                if (keepGoing) {
                    $scope.linkSelezionato.proprieta.push(
                        {nomeProp: $scope.newLinkProp.nome,
                            dominio: $scope.newLinkProp.dominio,
                            isPrimaryKey: $scope.newLinkProp.isPrimaryKey}
                    );
                }
                console.log($scope.linkSelezionato);
            };

            $scope.removeLinkProp = function (ind) {
                $scope.linkSelezionato.proprieta.splice(ind, 1);
            };

            //Inserimento degli intervalli per le propriietà
            $scope.insIntervallo = function () {
                //controllo estremi e definizione dell'intervallo
                if ($scope.sceltaVinc === "intervallo"){

                    if ($scope.interDa == null || $scope.interA == null || $scope.interDa === "" || $scope.interA === "") return;

                    if ($scope.interDa <= $scope.interA) {
                        if ($scope.estremoDaIncl && $scope.estremoAIncl) var intervallo = "value >= " + $scope.interDa + " and value <= " + $scope.interA;

                        else if ($scope.estremoDaIncl && !$scope.estremoAIncl) var intervallo = "value >= " + $scope.interDa + " and value < " + $scope.interA;

                        else if (!$scope.estremoDaIncl && $scope.estremoAIncl) var intervallo = "value > " + $scope.interDa + " and value <= " + $scope.interA;

                        else var intervallo = "value > " + $scope.interDa + " and value < " + $scope.interA;
                    }else{
                        alert("L'estremo inferiore deve essere minore o uguale di quello superiore");
                        return
                    }
                }
                else if ($scope.sceltaVinc === "maggiore"){
                    if ($scope.magDi == null || $scope.magDi === "") return;
                    if ($scope.estremoMag) var intervallo = "value >= " + $scope.magDi;
                    else var intervallo = "value > " + $scope.magDi;
                }
                else if ($scope.sceltaVinc === "minore"){
                    if ($scope.minDi == null || $scope.minDi === "") return;
                    if ($scope.estremoMin) var intervallo = "value <= " + $scope.minDi;
                    else var intervallo = "value < " + $scope.minDi;
                }

                // se il nuovo intervallo è gia presente non si inserisce
                if (($scope.linkSelezionato.proprieta[$scope.vincProp].valori.intervalli.length === 0 ||
                        !$scope.linkSelezionato.proprieta[$scope.vincProp].valori.intervalli.includes(intervallo)) &&
                    ($scope.interDa !== undefined || $scope.interA !== undefined || $scope.magDi !== undefined || $scope.minDi !== undefined))
                    $scope.linkSelezionato.proprieta[$scope.vincProp].valori.intervalli.push(intervallo);

                //reset campi degli intervalli
                $scope.interDa = null;
                $scope.estremoDaIncl = false;
                $scope.interA = null;
                $scope.estremoAIncl = false;
                $scope.magDi = null;
                $scope.estremoMag = false;
                $scope.minDi = null;
                $scope.estremoMin = false;
                console.log($scope.linkSelezionato);
            };

            // rimuove intervallo tramite indice
            $scope.removeIntervallo = function (ind) {
                $scope.linkSelezionato.proprieta[$scope.vincProp].valori.intervalli.splice(ind, 1);
            };

            // nasconde il modal
            $scope.closeDialog = function () {
                $mdDialog.hide();
            }
        }
    });