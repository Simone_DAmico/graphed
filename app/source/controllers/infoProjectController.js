app

// controller dei nodi del grafo; gestisce il model di ogni nodo
    .controller("infoProject", function($scope, $mdDialog, Labels){

        $scope.showInfoProject = function($event) {

            $mdDialog.show({
                targetEvent: $event,
                templateUrl: 'source/templates/infoProject.html',
                windowClass: 'modal-content',
                controller: projectInfoCtrl,
                clickOutsideToClose: true
            });
        };

        function projectInfoCtrl($scope, $mdDialog, Labels) {

            $scope.nomeProgetto = Labels.nomeProgetto;
            $scope.nomeDB = Labels.nomeDB;
            $scope.descrizioneProgetto = Labels.descrizioneProgetto;

            $scope.vedi = function () {
                console.log("scope",$scope.nomeProgetto);
                console.log("label",Labels.nomeProgetto);
                Labels.nomeProgetto = $scope.nomeProgetto;
                console.log("scope",$scope.nomeProgetto);
                console.log("label",Labels.nomeProgetto);
                console.log(Labels);
            };

            // Nasconde il modal
            $scope.closeDialog = function () {
                console.log("scope",$scope.nomeProgetto);
                console.log("label",Labels.nomeProgetto);

                $mdDialog.hide();
            };
        }


    });