app

// controller dei nodi del grafo; gestisce il model di ogni nodo
    .controller("typeController", function($scope, $mdDialog, Labels, $mdSidenav) {

        $scope.removeDom = function (ind) {
          Labels.domini.splice(ind, 1);
        };

        $scope.showInfo = function ($event, dom) {
            $mdDialog.show({
                locals:{dom: dom},
                targetEvent: $event,
                templateUrl: 'source/templates/infoTypeModal.html',
                windowClass: 'modal-content',
                controller: infoTypeCtrl,
                clickOutsideToClose: true
                }
            )
        };

        function infoTypeCtrl($scope, dom) {
            $scope.dom=dom;

            // rimuove intervallo tramite indice
            $scope.removeIntervallo = function (ind) {
                $scope.dom.valori.splice(ind, 1);
            };

            // nasconde il modal
            $scope.closeDialog = function () {
                $mdDialog.hide();
            }
        }

        $scope.openLeftMenu = function() {
            $mdSidenav('left').toggle();
        };

        $scope.domini = Labels.domini;

        $scope.addNewType = function($event) {
            $mdDialog.show({
                targetEvent: $event,
                templateUrl: 'source/templates/typeDefinationModal.html',
                windowClass: 'modal-content',
                controller: typeCtrl,
                clickOutsideToClose: true
            });
        };

        function typeCtrl($scope, $mdDialog, Labels) {
            $scope.tipiBase = Labels.tipiBase;
            $scope.domini = Labels.domini;
            $scope.intervalli=[];
            $scope.enum = [];
            var inserisci = true;
            $scope.newType = {};

            // resetta tutti campi
            $scope.reset = function () {
                $scope.newTypeDefault = null;
                $scope.interDa = null;
                $scope.interA = null;
                $scope.magDi = null;
                $scope.minDi = null;
                $scope.intervalli = [];
                $scope.enum = [];
                $scope.newType = {};
            };

            $scope.nuovoTipo= function () {

                console.log($scope.domini);
                // controllo se il nome è di un tipo già definito
                Labels.domini.forEach(function (t) {
                    if (t.nome === $scope.domini) {
                        alert("il tipo con nome " + t.nome + " esiste già");
                        inserisci = false;
                    }
                });

                if ($scope.newTypeGen === "enum" && $scope.enum.length === 0) {
                    inserisci = false;
                    alert("Inserire almeno un elemento nella lista");
                }
                console.log($scope.sel);

                // se ok --> inserimento
                if (inserisci){
                    if ($scope.newTypeGen === "enum"){
                        if ($scope.sel === undefined)
                            var newType = {nome: $scope.newTypeName, generico: $scope.newTypeGen, default: null, valori: $scope.enum};
                        else
                            var newType = {nome: $scope.newTypeName, generico: $scope.newTypeGen, default: $scope.enum[$scope.sel], valori: $scope.enum};
                    }
                    else{
                        if ($scope.newTypeDefault === undefined || $scope.newTypeDefault === "" || $scope.newTypeDefault === null)
                            var newType = {nome: $scope.newTypeName, generico: $scope.newTypeGen,
                                default: null, valori: $scope.intervalli};
                        else
                            var newType = {nome: $scope.newTypeName, generico: $scope.newTypeGen,
                                default: $scope.newTypeDefault, valori: $scope.intervalli};
                    }
                    Labels.domini.push(newType);
                    $scope.reset();
                    $scope.newTypeName = null;
                }
                console.log($scope.domini);
            };

            $scope.insIntervalli = function () {
                console.log($scope.intervalli);
                //controllo estremi e definizione dell'intervallo
                if ($scope.sceltaVinc === "intervallo"){
                    if ($scope.interDa <= $scope.interA) {
                        if ($scope.estremoDaIncl && $scope.estremoAIncl) var intervallo = "value >= " + $scope.interDa + " and value <= " + $scope.interA;

                        else if ($scope.estremoDaIncl && !$scope.estremoAIncl) var intervallo = "value >= " + $scope.interDa + " and value < " + $scope.interA;

                        else if (!$scope.estremoDaIncl && $scope.estremoAIncl) var intervallo = "value > " + $scope.interDa + " and value <= " + $scope.interA;

                        else var intervallo = "value > " + $scope.interDa + " and value < " + $scope.interA;
                    }else{
                        alert("L'estremo inferiore deve essere minore o uguale di quello superiore");
                        return
                    }
                }
                else if ($scope.sceltaVinc === "maggiore"){
                    if ($scope.estremoMag) var intervallo = "value >= " + $scope.magDi;
                    else var intervallo = "value > " + $scope.magDi;
                }
                else if ($scope.sceltaVinc === "minore"){
                    if ($scope.estremoMin) var intervallo = "value <= " + $scope.minDi;
                    else var intervallo = "value < " + $scope.minDi;
                }

                // se il nuovo intervallo è gia presente non si inserisce
                if (($scope.intervalli.length === 0 || !$scope.intervalli.includes(intervallo)) &&
                    ($scope.interDa !== null || $scope.interA !== null || $scope.magDi !== null || $scope.minDi !== null))
                    $scope.intervalli.push(intervallo);

                //reset campi degli intervalli
                $scope.interDa = null;
                $scope.estremoDaIncl = false;
                $scope.interA = null;
                $scope.estremoAIncl = false;
                $scope.magDi = null;
                $scope.estremoMag = false;
                $scope.minDi = null;
                $scope.estremoMin = false;
            };

            // rimuove intervallo tramite indice
            $scope.removeIntervallo = function (ind) {
                $scope.intervalli.splice(ind, 1);
            };

            // GESTIONE DELLE LISTE DI ELEMENTI PREFISSATI

            // aggiunge l'elemento alla lista
            $scope.addElem = function() {
                if ($scope.nomeApp === undefined || $scope.nomeApp === null || $scope.nomeApp === ""){
                    return;
                }
                else if (!$scope.enum.includes($scope.nomeApp)){
                    $scope.enum.push($scope.nomeApp);
                }
                else {
                    alert("Elemento giè esistente");
                    return
                }
                $scope.nomeApp = undefined;
            };

            // rimuove l'elemento tramite indice
            $scope.removeIntervallo = function (ind) {
                $scope.enum.splice(ind, 1);
            };

            // nasconde il modal
            $scope.closeDialog = function () {
                $mdDialog.hide();
            }
        }
    });